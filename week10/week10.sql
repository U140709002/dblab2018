describe Customers;

select *
from Customers
order by CustomerName desc;

create index abc on Customers(CustomerName);

explain select *
from Customers
order by CustomerName desc;

alter table Customers drop index abc;

create view myview as select *
from Customers;

select * from myview;

insert into myview values(347,"mugla","a","s","d",713,"f");

select * from Customers;