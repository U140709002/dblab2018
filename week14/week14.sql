create table `employees_backup` (
    `id` INT auto_increment,
    `EmployeeID` int(11) not null,
    `LastName` varchar(45) not null,
    `FirstName` varchar(45) not null,
    `BirthDate` varchar(45) not null,
    `changedat` datetime default null,
    `action` varchar(50) default null,
    primary key (id)
);

select * from employees_backup;

update employees set LastName = "Turk" where EmployeeID = 6;

update employees set BirthDate = "1999-01-01" where EmployeeID = 8;

insert into  employees (LastName,FirstName,Birthdate,Photo,Notes)
	values ("Turk","Erdem","1990-01-01", "EmpID11.pic","Intern");
    
delete from employees where EmployeeID = 11;