select count(customers.CustomerID) as numberOfCustomers, customers.Country
from customers
group by customers.Country
order by numberOfCustomers desc;

select products.ProductID, suppliers.SupplierName
from products join suppliers on products.SupplierID = suppliers.SupplierID
group by products.SupplierID
order by count(products.ProductID) desc;

load data local infile "C:\\Users\\Gülderen\\Desktop\\customers.csv"
into table customers
fields terminated by ';'
ignore 1 lines;

select * from customers;